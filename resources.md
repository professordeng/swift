---
title: resources
---

[首页](/) | [目录]({{site.baseurl}}) | [资源]({{site.baseurl}}/resources)

---

相比 objc，swift 现代、直观、简洁。

1. [swift 5 中文手册](https://www.runoob.com/manual/gitbook/swift5/source/_book/index.html) : 抗拒英语的可以先读这个（不过最后还是要读英语文档）
2. [swift 官方文档](https://docs.swift.org/swift-book/GuidedTour/GuidedTour.html) : 入门后精读
3. [苹果人机交互 - 图标篇](https://developer.apple.com/design/human-interface-guidelines/ios/icons-and-images/custom-icons/)
4. [小波说雨燕](http://xiaoboswift.com/) : 快速入门
5. [swift online](http://online.swiftplayground.run/)
6. [kiloloco](https://www.kiloloco.com/) ：YouTuber，讲课超好。